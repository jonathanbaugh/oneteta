# One Te Ta

One Te Ta is a drum exercise generator to help you practice random patterns and avoid the same old exercises.

## Quick start

```bash
npm install
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and... Eyeball!

### Build

```bash
npm run build
```
